from django.contrib import admin

from . import models


class ClimatoJournaliereAdmin(admin.ModelAdmin):
    search_fields = ['id_station','tx','tn']
    list_display = (
        'id_station',
        'jour',
        'mois',
        'annee',
        'tn',
        'tx',
        'rr',
        'ens',
    )


class HistoricEventsAdmin(admin.ModelAdmin):
    list_filter=["importance", 'date_deb', 'date_fin']
    search_fields=["localisation"]
    list_display = (
        'id',
        'nom',
        'localisation',
        'importance',
        'type_cyclone',
        'has_image_cyclone',
        'date_deb',
        'date_fin',
        'duree',
        'type',
        'description',
        'short_desc',
        'sources',
        'id_compte',
        'valeur_max',
        'bs_link',
        'gen_cartes',
        'why',
        'tableau_croise',
        'tableau_croise_cyclone',
        'hits',
        'notes',
    )


class HistoricNormalesAdmin(admin.ModelAdmin):
    search_fields=["geoid"]
    list_display = (
        'geoid',
        'mois',
        'tx',
        'tn',
        'precip',
        'altitude_ref',
        'nom_ref',
        'dept_ref',
        'distance',
        'wmo_ref',
    )


class HistoricValuesAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'id_historic',
        'lieu',
        'geoid',
        'dept',
        'pays',
        'valeur',
        'type',
        'date',
        'commentaire',
        'est_record',
        'est_record_dpt',
    )
    list_filter = ('date',)


class StationsAdmin(admin.ModelAdmin):
    search_fields = ['id','uniqueid','departement']
    list_display = (
        'uniqueid',
        'id',
        'genre',
        'libelle',
        'departement',
        'pays',
        'latitude',
        'longitude',
        'altitude',
        'orientation',
        'station_reference',
        'pas_de_synop',
        'climato_only',
        'dh_min_climato',
        'dh_min_live',
        'dh_ouverture',
        'base_climato',
        'last_report',
        'last_data',
    )
    list_filter = (
        'dh_min_climato',
        'dh_min_live',
        'dh_ouverture',
        'last_report',
    )


class PhotosEsgiAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'id_compte',
        'photo_url',
        'dh_publication',
        'dh_prise',
        'lieu',
        'ville',
        'departement',
        'pays',
        'latitude',
        'longitude',
        'altitude',
        'id_geonames',
        'titre',
        'description',
        'message_aux_moderateurs',
        'exif',
        'statut',
        'accord_media',
        'libre_droits',
        'accord_evaluations',
        'dh_envoi',
        'id_moderateur',
        'id_mail_refus',
        'avis_moderateurs_demande',
        'tag',
        'tags',
        'precision_lieu',
        'est_argentic',
        'est_mobile',
        'likes_cache',
    )
    list_filter = ('dh_publication', 'dh_prise', 'dh_envoi')


def _register(model, admin_class):
    admin.site.register(model, admin_class)


_register(models.ClimatoJournaliere, ClimatoJournaliereAdmin)
_register(models.HistoricEvents, HistoricEventsAdmin)
_register(models.HistoricNormales, HistoricNormalesAdmin)
_register(models.HistoricValues, HistoricValuesAdmin)
_register(models.PhotosEsgi, PhotosEsgiAdmin)
_register(models.Stations, StationsAdmin)

