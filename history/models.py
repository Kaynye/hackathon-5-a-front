# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class ClimatoJournaliere(models.Model):
    id_station = models.CharField(primary_key=True, max_length=15)
    jour = models.PositiveIntegerField()
    mois = models.PositiveIntegerField()
    annee = models.PositiveSmallIntegerField()
    tn = models.FloatField(blank=True, null=True)
    tx = models.FloatField(blank=True, null=True)
    rr = models.FloatField(blank=True, null=True)
    ens = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'climato_journaliere'
        unique_together = (('id_station', 'jour', 'mois', 'annee'),)


class HistoricEvents(models.Model):
    nom = models.CharField(max_length=255)
    localisation = models.CharField(max_length=255)
    importance = models.IntegerField()
    type_cyclone = models.CharField(max_length=100)
    has_image_cyclone = models.IntegerField()
    date_deb = models.DateField()
    date_fin = models.DateField()
    duree = models.IntegerField()
    type = models.CharField(max_length=255)
    description = models.TextField()
    short_desc = models.TextField()
    sources = models.TextField()
    id_compte = models.IntegerField()
    valeur_max = models.FloatField()
    bs_link = models.IntegerField()
    gen_cartes = models.IntegerField()
    why = models.TextField()
    tableau_croise = models.TextField()
    tableau_croise_cyclone = models.TextField()
    hits = models.IntegerField()
    notes = models.TextField()

    class Meta:
        managed = False
        db_table = 'historic_events'


class HistoricNormales(models.Model):
    geoid = models.PositiveBigIntegerField(primary_key=True)
    mois = models.PositiveIntegerField()
    tx = models.FloatField()
    tn = models.FloatField()
    precip = models.FloatField()
    altitude_ref = models.FloatField()
    nom_ref = models.CharField(max_length=100)
    dept_ref = models.CharField(max_length=3)
    distance = models.FloatField()
    wmo_ref = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'historic_normales'
        unique_together = (('geoid', 'mois'),)


class HistoricValues(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_historic = models.PositiveIntegerField()
    lieu = models.CharField(max_length=255)
    geoid = models.PositiveIntegerField()
    dept = models.CharField(max_length=255)
    pays = models.CharField(max_length=255)
    valeur = models.FloatField()
    type = models.IntegerField()
    date = models.DateTimeField()
    commentaire = models.CharField(max_length=255)
    est_record = models.IntegerField()
    est_record_dpt = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'historic_values'
        unique_together = (('date', 'geoid', 'type', 'id_historic'),)


class PhotosEsgi(models.Model):
    id_compte = models.IntegerField()
    photo_url = models.CharField(max_length=48)
    dh_publication = models.DateTimeField()
    dh_prise = models.DateTimeField()
    lieu = models.CharField(max_length=100)
    ville = models.CharField(max_length=100, blank=True, null=True)
    departement = models.CharField(max_length=3, blank=True, null=True)
    pays = models.CharField(max_length=50, blank=True, null=True)
    latitude = models.FloatField()
    longitude = models.FloatField()
    altitude = models.CharField(max_length=12)
    id_geonames = models.IntegerField()
    titre = models.CharField(max_length=255)
    description = models.TextField()
    message_aux_moderateurs = models.CharField(max_length=255)
    exif = models.CharField(max_length=255, blank=True, null=True)
    statut = models.CharField(max_length=2)
    accord_media = models.CharField(max_length=1)
    libre_droits = models.CharField(max_length=1)
    accord_evaluations = models.CharField(max_length=1)
    dh_envoi = models.DateTimeField()
    id_moderateur = models.IntegerField()
    id_mail_refus = models.IntegerField(blank=True, null=True)
    avis_moderateurs_demande = models.CharField(max_length=1)
    tag = models.CharField(max_length=1)
    tags = models.TextField()
    precision_lieu = models.CharField(max_length=255)
    est_argentic = models.PositiveIntegerField()
    est_mobile = models.PositiveIntegerField()
    likes_cache = models.TextField()

    class Meta:
        managed = False
        db_table = 'photos_esgi'


class Stations(models.Model):
    uniqueid = models.BigAutoField(primary_key=True)
    id = models.CharField(unique=True, max_length=15)
    genre = models.CharField(max_length=6)
    libelle = models.CharField(max_length=50)
    departement = models.CharField(max_length=3)
    pays = models.CharField(max_length=3)
    latitude = models.FloatField()
    longitude = models.FloatField()
    altitude = models.PositiveSmallIntegerField()
    orientation = models.IntegerField()
    station_reference = models.CharField(max_length=5)
    pas_de_synop = models.SmallIntegerField()
    climato_only = models.PositiveIntegerField()
    dh_min_climato = models.DateField(blank=True, null=True)
    dh_min_live = models.DateField(blank=True, null=True)
    dh_ouverture = models.DateField(blank=True, null=True)
    base_climato = models.PositiveIntegerField()
    last_report = models.DateTimeField()
    last_data = models.CharField(max_length=255)

    class Meta:
        managed = False
        db_table = 'stations'