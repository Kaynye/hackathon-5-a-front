from django.views import View
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.utils.decorators import method_decorator
from .models import *
from django.core.paginator import Paginator
from django.db.models import Max,Min

@method_decorator(csrf_exempt, name="put")
class ClimatoJournaliereView(View):
    
    def get(self, request):
        res={"data":[]}
        listClim= ClimatoJournaliere.objects.all()
        paginator = Paginator(listClim, 20)
        page_obj = paginator.get_page(1)
        for climat in page_obj:
            res["data"].append({
                "idStation" : climat.id_station,
                "jour" : climat.jour,
                "mois" : climat.mois,
                "annee" : climat.annee,
                "tn" : climat.tn,
                "tx" : climat.tx,
                "rr" : climat.rr,
                "ens" : climat.ens,
            })
        return JsonResponse(res,safe=False)
    
    
    def put(self,request):
        res={"data":[]}

        return JsonResponse(res,safe=False)
    
    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(ClimatoJournaliereView, self).dispatch(*args, **kwargs)


class HistoricValuesView(View):
    def get(self, request):
        res = {"datas": []}
        list_historic_values = HistoricValues.objects.all()
        paginator = Paginator(list_historic_values, 20)
        page_obj = paginator.get_page(1)
        for hist_val in page_obj:
            res["datas"].append({
                "id": hist_val.id,
                "id_historic": hist_val.id_historic,
                "lieu": hist_val.lieu,
                "geoid": hist_val.geoid,
                "dept": hist_val.dept,
                "pays": hist_val.pays,
                "valeur": hist_val.valeur,
                "type": hist_val.type,
                "date": hist_val.date,
                "commentaire": hist_val.commentaire,
                "est_record": hist_val.est_record,
                "est_record_dpt": hist_val.est_record_dpt,
            })
        return JsonResponse(res, safe=False)


class StationsView(View):
    def get(self, request):
        res = {"datas": []}
        stations = Stations.objects.all()
        paginator = Paginator(stations, 20)
        page_obj = paginator.get_page(1)
        for station in page_obj:
            res["datas"].append({
                "uniqueid": station.uniqueid,
                "id": station.id,
                "genre": station.genre,
                "libelle": station.libelle,
                "departement": station.departement,
                "pays": station.pays,
                "latitude": station.latitude,
                "longitude": station.longitude,
                "altitude": station.altitude,
                "orientation": station.orientation,
                "station_reference": station.station_reference,
                "pas_de_synop": station.pas_de_synop,
                "climato_only":station.climato_only,
                "dh_min_climato": station.dh_min_climato,
                "dh_min_live": station.dh_min_live,
                "dh_ouverture": station.dh_ouverture,
                "base_climato": station.base_climato,
                "last_report": station.last_report,
                "last_data": station.last_data,
            })
        return JsonResponse(res, safe=False)


class HistoricEventsView(View):
    def get(self, request):
        res = {"datas": []}
        historic_events = HistoricEvents.objects.all().order_by("-date_deb")
        paginator = Paginator(historic_events, 20)
        page_obj = paginator.get_page(1)
        for historic_event in page_obj:
            res["datas"].append({
                "nom": historic_event.nom,
                "localisation": historic_event.localisation,
                "importance": historic_event.importance,
                "type_cyclone": historic_event.type_cyclone,
                "has_image_cyclone": historic_event.has_image_cyclone,
                "date_deb": historic_event.date_deb,
                "date_fin": historic_event.date_fin,
                "duree": historic_event.duree,
                "type": historic_event.type,
                "description": historic_event.description,
                "short_desc": historic_event.short_desc,
                "sources": historic_event.sources,
                "id_compte": historic_event.id_compte,
                "valeur_max": historic_event.valeur_max,
                "bs_link": historic_event.bs_link,
                "gen_cartes": historic_event.gen_cartes,
                "why": historic_event.why,
                "tableau_croise": historic_event.tableau_croise,
                "tableau_croise_cyclone": historic_event.tableau_croise_cyclone,
                "hits": historic_event.hits,
                "notes": historic_event.notes,
            })
        return JsonResponse(res, safe=False)



class HistoricEventsByHitsView(View):
    def get(self, request):
        res = {"datas": []}
        historic_events = HistoricEvents.objects.all().order_by("-hits")
        paginator = Paginator(historic_events, 20)
        page_obj = paginator.get_page(1)
        for historic_event in page_obj:
            res["datas"].append({
                "nom": historic_event.nom,
                "localisation": historic_event.localisation,
                "importance": historic_event.importance,
                "type_cyclone": historic_event.type_cyclone,
                "has_image_cyclone": historic_event.has_image_cyclone,
                "date_deb": historic_event.date_deb,
                "date_fin": historic_event.date_fin,
                "duree": historic_event.duree,
                "type": historic_event.type,
                "description": historic_event.description,
                "short_desc": historic_event.short_desc,
                "sources": historic_event.sources,
                "id_compte": historic_event.id_compte,
                "valeur_max": historic_event.valeur_max,
                "bs_link": historic_event.bs_link,
                "gen_cartes": historic_event.gen_cartes,
                "why": historic_event.why,
                "tableau_croise": historic_event.tableau_croise,
                "tableau_croise_cyclone": historic_event.tableau_croise_cyclone,
                "hits": historic_event.hits,
                "notes": historic_event.notes,
            })
        return JsonResponse(res, safe=False)


class HistoricNormalesView(View):
    def get(self, request):
        res = {"datas": []}
        stations = HistoricNormales.objects.all()
        paginator = Paginator(stations, 192)
        page_obj = paginator.get_page(1)
        for historic_normale in page_obj:
            res["datas"].append({
                "geoid": historic_normale.geoid,
                "mois": historic_normale.mois,
                "tx": historic_normale.tx,
                "tn": historic_normale.tn,
                "precip": historic_normale.precip,
                "altitude_ref": historic_normale.altitude_ref,
                "nom_ref": historic_normale.nom_ref,
                "dept_ref": historic_normale.dept_ref,
                "distance": historic_normale.distance,
                "wmo_ref": historic_normale.wmo_ref
            })
        return JsonResponse(res, safe=False)
    
def getInfoByDepats(request):
    print(request.body)
    print(request.GET.get("liste").split(","))
    res=[]
    listeDept=request.GET.get("liste").split(",")
    if listeDept[0]!="":
        for dept in listeDept:
            stations=list(map( lambda station: station.id ,Stations.objects.filter(departement=dept)))
            print(stations)
            res.append({
                "tx__max":ClimatoJournaliere.objects.filter(id_station__in=stations).aggregate(Max('tx'))['tx__max'],
                "tn__min":ClimatoJournaliere.objects.filter(id_station__in=stations).aggregate(Min('tn'))['tn__min'],
                "count":HistoricEvents.objects.filter(localisation__contains=dept).count(),
                "anecdotique":HistoricEvents.objects.filter(localisation__contains=dept,importance=1).count(),
                "notable":HistoricEvents.objects.filter(localisation__contains=dept,importance=2).count(),
                "remarquable":HistoricEvents.objects.filter(localisation__contains=dept,importance=3).count(),
                "exceptionnel":HistoricEvents.objects.filter(localisation__contains=dept,importance=4).count(),
                "memorable":HistoricEvents.objects.filter(localisation__contains=dept,importance=5).count(),
                "listeLien":list(map(lambda photo: {"titre": photo.titre, "ville": photo.ville, "lien":photo.dh_publication.strftime("%Y-%m")+"/"+photo.photo_url} ,PhotosEsgi.objects.filter(departement=dept).order_by("-dh_publication")[:10])),
                "id":dept,
            })
 
    return JsonResponse(res, safe=False)

def getInfoOfToday(request):
    HistoricEvents.objects.filter("date_deb")
    listeDept=request.GET.get("liste").split(",")
    res = []
    if listeDept[0]!="":
        for dept in listeDept:
            # HistoricValues.objects.filter(dept=dept).aggregate(Max('tx')
            # res[dept]={
            #     "tx__max": HistoricValues.objects.filter(dept=dept).aggregate(Max('tx'))['tx__max'],
            #     "tn__min": HistoricValues.objects.filter(dept=dept).aggregate(Max('tn'))['tn__min']
            # }
            stations=list(map( lambda station: station.id ,Stations.objects.filter(departement=dept)))
            print(stations)
            res.append({
                "tx__max":ClimatoJournaliere.objects.filter(id_station__in=stations).aggregate(Max('tx'))['tx__max'],
                "tn__min":ClimatoJournaliere.objects.filter(id_station__in=stations).aggregate(Min('tn'))['tn__min'],
                "id":dept,
        })
 
    return JsonResponse(res, safe=False)